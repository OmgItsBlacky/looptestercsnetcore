﻿using System;

namespace LoopTesting
{
    internal class TestingArea
    {
        public static int[]  LongForLoopBubble(int[] data, ulong loopRange)
        {
            ulong i = 0;
            int temporary = 0;
    
            for (; i < (loopRange - 1); i++)
            {
                if(data[i] < data[i+1])
                {
                    temporary = data[i];
                    data[i] = data[i+1];
                    data[i+1] = temporary;
                }
            }

            return data;
        }

        public static int[]  LongForLoopBubbleOptimized(int[] data, ulong loopRange)
        {
            ulong i = 0;
            
            int temporary_A = 0;
            int temporary_B = 0;
        
            for (; i < loopRange - 1; i++)
            {
                temporary_A = data[i];
                temporary_B = data[i+1];
            
                data[i] = temporary_B > temporary_A ? temporary_B : temporary_A;
                data[i+1] = temporary_B > temporary_A ? temporary_A : temporary_B;
            }

            return data;
        }

        public static void Main(string[] args)
        {
            const ulong dataSize = 1000000000;
            const ulong numberOfLoopExecutions = 10;
            
            var loopTesters = new LoopTester[]
            {
                new LoopTester(LongForLoopBubble, numberOfLoopExecutions, dataSize-1, dataSize),
                new LoopTester(LongForLoopBubble, numberOfLoopExecutions, dataSize-1, dataSize),
                new LoopTester(LongForLoopBubbleOptimized, numberOfLoopExecutions, dataSize-1, dataSize),
                new LoopTester(LongForLoopBubbleOptimized, numberOfLoopExecutions, dataSize-1, dataSize),
            };

            foreach (var loopTester in loopTesters)
            {
                loopTester.RunTest();
            }
            
            new CSVFileGenerator(@"./", loopTesters);
        }
    }
}